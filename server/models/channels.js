"use strict";

class Channels {
    constructor(id, name, groupId, users) {
        this.id = id;
        this.name = name;
        this.groupId = groupId;
        this.users = users;
    }

  static loadFromPost(body) {
    return new Channels(body.id, body.name, body.groupId, body.users);
  }

}

module.exports = Channels;