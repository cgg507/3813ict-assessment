"use strict";

class Sessions {
  constructor(session, userId) {
    this.session = session;
    this.userId = userId;
  }
}

module.exports = Sessions;