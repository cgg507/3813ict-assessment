"use strict";

class ApiResponse {
    constructor(props) {
        this.success = false;
        this.data = null;
        this.errors = null;
    }

};

module.exports = ApiResponse;