"use strict";

class Users {
    constructor(id, email, firstName, lastName, permissions) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.permissions = permissions;
    }
    
    static loadFromPost(body) {
        return new Users(body.id, body.email, body.firstName, body.lastName);
    }

}

module.exports = Users;