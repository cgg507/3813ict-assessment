"use strict";

class Groups {
    constructor(id, name, admins, users) {
        this.id = id;
        this.name = name;
        this.admins = admins;
        this.users = users;
    }

    validate() {
        let validation = [
          ['id', 'number', true],
          ['name', 'string', true],
          ['admins', 'array', false],
          ['users', ]
        ];

    }

  static loadFromPost(body) {
    return new Groups(body.id, body.name, body.admins, body.users);
  }

}

module.exports = Groups;