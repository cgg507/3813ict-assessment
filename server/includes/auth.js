'use strict';

const ApiResponse = require("../models/api_response");
const GROUP_PERM = 'group';
const ADMIN_PERM = 'admin';

const atob = require('atob');

module.exports.isAuthenticated = function(req, res, next) {
  //Check for the header
  if (typeof(req.headers.authorization) !== undefined && req.headers.authorization != null) {
    const auth = req.headers.authorization.split(" ");
    if (auth.length === 2 && auth[1] !== "") {
      let session = atob(auth[1]);
      if (req.sessionService.checkSession(session)) {
        req.sessionToken = session;
        req.thisUser = req.userService.getUserByToken(req.sessionToken);
        return next();
      }
    }
  }
  let resp = new ApiResponse();
  resp.error = "Authentication not accepted";
  res.send(resp);
};

module.exports.hasGroupWrite = function(req, res, next) {
  /** @var thisUser Users */
  let thisUser = req.userService.getUserByToken(req.sessionToken);
  if (thisUser.permissions.includes(ADMIN_PERM)) {
    return next();
  }
  if (thisUser.permissions.includes(GROUP_PERM)) {
    let id = parseInt(req.params['id']);
    if (id > 0) {
      let g = req.groupService.getGroup(id);
      if (g.admins.includes(thisUser.id)) {
        return next();
      }
    } else {
      return next();
    }
  }

  let resp = new ApiResponse();
  resp.error = "Permission denied";
  res.send(resp);
};

module.exports.hasChannelWrite = function(req, res, next) {
  /** @var thisUser Users */
  let thisUser = req.userService.getUserByToken(req.sessionToken);
  if (thisUser.permissions.includes(ADMIN_PERM)) {
    return next();
  }
  if (thisUser.permissions.includes(GROUP_PERM)) {
    let id = parseInt(req.params['id']);
    if (id > 0) {
      let c = req.channelService.getChannel(id);
      let g = req.groupService.getGroup(c.groupId);
      if (g.admins.includes(thisUser.id)) {
        return next();
      }
    }
  }

  let resp = new ApiResponse();
  resp.error = "Permission denied";
  res.send(resp);
};

module.exports.hasUserWrite = function(req, res, next) {
  /** @var thisUser Users */
  let thisUser = req.userService.getUserByToken(req.sessionToken);
  if (thisUser.permissions.includes(ADMIN_PERM)) {
    return next();
  }
  // Let users edit themselves
  let id = parseInt(req.params['id']);
  if (thisUser.id == id) {
    return next();
  }

  let resp = new ApiResponse();
  resp.error = "Permission denied";
  res.send(resp);
};