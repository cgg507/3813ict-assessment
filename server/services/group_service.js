"use strict";

const User = require("../models/users");



module.exports = function groupService(db) {
  return {
    getGroups: () => {
      return db.groupsArray;
    },
    getGroup: (id) => {
      return db.groupsArray.find((ele) => {
        return ele.id === id;
      });
    },
    
    /**
     *
     * @param user
     * @param callback
     */
    insertGroup: (group) => {
      group.id = (new Date()).getTime();

      db.groupsArray.push(group);
      return true;
    },

    updateGroup: (group) => {
      let oldData = db.groupsArray.find((ele) => {
        return ele.id == group.id;
      });

      if (typeof(oldData) === 'undefined') {
        console.error("Group does not exist");
        return false;
      }

      oldData.name = group.name;
      oldData.users = group.users == null ? [] : group.users;
      oldData.admins = group.admins == null ? [] : group.admins;
      return true;
    },

    deleteGroup: (id) => {
      let index = db.groupsArray.findIndex((ele) => {
        return ele.id == id;
      });

      try {
        console.log("Removing group at index " + index);
        db.groupsArray.splice(index, 1);
        return true;
      } catch (err) {
        return false;
      }
    },
  };
};
