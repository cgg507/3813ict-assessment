"use strict";

let Users = require("../models/users");
let Groups = require("../models/groups");
let Channels = require("../models/channels");

const BASE_PATH = "./data/";

var fs = require('fs');

class FakeDB {
  constructor() {
    this.userArray = [];
    this.groupsArray = [];
    this.channelsArray = [];
      /** @var sessionsArray [Sessions] */
    this.sessionsArray = [];
  };

  loadDatabase() {
    function readFakeDB (file) {
      try {
        let data = fs.readFileSync(BASE_PATH + file, 'utf8');
        let parsedData = JSON.parse(data);
        return parsedData;
      } catch(err) {
        throw err;
      }
    }

    this.userArray = readFakeDB('users.json');
    this.groupsArray = readFakeDB('groups.json');
    this.channelsArray = readFakeDB('channels.json');
    this.sessionsArray = readFakeDB('sessions.json');
  };

  saveDatabase() {
    function writeFakeDB(file, arr) {
      let d = JSON.stringify(arr);
      fs.writeFile(BASE_PATH + "/" + file, d, function (err) {
        if (err) {
          throw err;
        }
        return true;
      });
    }

    writeFakeDB('users.json', this.userArray);
    writeFakeDB('groups.json', this.groupsArray);
    writeFakeDB('channels.json', this.channelsArray);
    writeFakeDB('sessions.json', this.sessionsArray);
  };

  insertDB(obj, toJson) {
    //Check whether it exists first
    let key = getAutoIncrement();

  };

  updateDB(key, obj, toJson) {

  };
}

module.exports = function() { return new FakeDB(); };