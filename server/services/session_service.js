"use strict";

const Sessions = require('../models/sessions');

module.exports = function sessionService(db) {
  return {
    checkSession: (session) => {
      return db.sessionsArray.find((ele) => {
        return ele.session == session;
      });
    },

    insertSession: (userId, session, callback) => {
      try {
        let newSession = new Sessions(session, userId);

        console.log("Creating new session:" + session);
        db.sessionsArray.push(newSession);
        callback(true);
      } catch(err) {
        console.error(err.message);
        callback(false);
      }
    },
    
    removeSession(userId, session, callback) {
      let index = db.sessionsArray.findIndex((ele) => {
        return ele.userId == userId && ele.session == session;
      });
      
      try {
        console.log("Removing session at index " + index);
        db.sessionsArray.splice(index, 1);
        callback(true);
      } catch (err) {
        callback(false);
      }
    }

  };
};
