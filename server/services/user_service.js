"use strict";

module.exports = function userService(db) {
  return {
    getUsers: () => {
      return db.userArray;
    },
    getUser: (id) => {
      return db.userArray.find((ele) => {
        return parseInt(ele.id) === parseInt(id);
      });
    },

    getUserByToken: (token) => {
      let session = db.sessionsArray.find((ele) => {
        return ele.session == token;
      });
      if (typeof(session) !== 'undefined') {
        return db.userArray.find((ele) => {
          return ele.id === session.userId;
        });
      }
      return null;
    },

    findByEmail: (email) => {
      return db.userArray.find(function(ele) {
        return ele.email === email;
      });
    },

    getUsersForGroup: (groupId) => {
      let g = db.groupsArray.find(function(ele) {
        return ele.id === groupId;
      });
      if (g !== null) {
        //g = g as Groups;
        //Filter the users against the group users
        let usersList = db.userArray.filter(function(ele) {
          let find = g.users.findIndex(function(gEle) {
            return ele.id == gEle;
          });
          return find !== -1;
        });
        return usersList;
      }
      return [];
    },


    /**
     *
     * @param user
     */
    insertUser: (user) => {
      user.id = (new Date()).getTime();
      user.permissions = user.permissions == null ? [] : user.permissions;
      db.userArray.push(user);
      return true
    },

    updateUser: (user) => {
      let oldData = db.userArray.find((ele) => {
        return ele.id == user.id;
      });

      if (typeof(oldData) === 'undefined') {
        console.error("User does not exist");
        return false;
      }

      oldData.email = user.email;
      oldData.firstName = user.firstName;
      oldData.lastName = user.lastName;
      oldData.permissions = user.permissions == null ? [] : user.permissions;
      return true;
    },
    
    deleteUser: (id) => {
      let index = db.userArray.findIndex((ele) => {
        return ele.id == id;
      });

      try {
        console.log("Removing session at index " + index);
        db.userArray.splice(index, 1);
        return true;
      } catch (err) {
        return false;
      }
    },
  };
};
