"use strict";

const User = require("../models/users");

module.exports = function channelService(db) {
  return {
    getChannels: () => {
      return db.channelsArray;
    },
    getChannelsForGroup: (groupId) => {
      return db.channelsArray.filter(function(ele) {
        return ele.groupId = groupId;
      });
    },
    getChannel: (id) => {
      return db.channelsArray.find((ele) => {
        return parseInt(ele.id) === parseInt(id);
      });
    },
    
    insertChannel: (channel) => {
      channel.id = (new Date()).getTime();
      db.channelsArray.push(channel);
      return true;
    },

    updateChannel: (channel) => {
      let oldData = db.channelsArray.find((ele) => {
        return ele.id == channel.id;
      });
      oldData.name = channel.name;
      oldData.groupId = channel.groupId;
      oldData.users  = channel.users == null ? [] : channel.users;
      return true;
    },

    deleteChannel: (id) => {
      let index = db.channelsArray.findIndex((ele) => {
        return ele.id == id;
      });

      try {
        console.log("Removing channel at index " + index);
        db.channelsArray.splice(index, 1);
        return true;
      } catch (err) {
        return false;
      }
    },
  };
};
