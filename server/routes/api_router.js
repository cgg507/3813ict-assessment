"use strict";

const express = require('express');
const router = express.Router();
const ApiResponse = require("../models/api_response");
const isAuthenticated = require("../includes/auth").isAuthenticated;

module.exports = function () {
  router.post('/login', function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /login post");
    res.setHeader('Content-Type', 'application/json');
    let un = req.body.username;
    let pw = req.body.password;
    let data = req.userService.findByEmail(un);
    if (data != null) {
      let randomKey = data.id + new Date().getTime();
      req.sessionService.insertSession(data.id, randomKey, (success) => {
        if (success) {
          response.data = {'session': randomKey, 'user': data};
          response.success = true;
          res.send(response);
        } else {
          response.success = false;
          res.send(response);
        }
      });
    } else {
      response.success = false;
      res.send(response);
    }
  });

  router.get('/logout', isAuthenticated, function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    let response = new ApiResponse();
    console.log("Request: /logout get");
    const userToken = req.sessionToken;

    let user = req.userService.getUserByToken(userToken);
    if (user !== null) {
      req.sessionService.removeSession(user.id, userToken, (success) => {
        if (success) {
          response.data = null;
          response.error = true;
        }
        res.send();
      });
    } else {
      response.errors = ["Failed to find user"]
      response.success = false;
      res.send(response);
    }

  });

  return router;
};