"use strict";

const express = require('express');
const router = express.Router();
const ApiResponse = require("../models/api_response");
const isAuthenticated = require("../includes/auth").isAuthenticated;
const hasUserWrite = require("../includes/auth").hasUserWrite;
const Users = require("../models/users");


module.exports = function (db) {
  router.get('/', isAuthenticated, function (req, res) {
    console.log("Request: /user get");
    res.setHeader('Content-Type', 'application/json');
    let response = new ApiResponse();
    response.data = req.userService.getUsers();
    response.success = true;

    res.send(response);
  });

  router.get('/:id', isAuthenticated, function (req, res) {
    console.log("Request: /user:id get");
    res.setHeader('Content-Type', 'application/json');
    let response = new ApiResponse();
    let user = req.userService.getUser(req.params['id']);
    if (typeof(user) !== 'undefined') {
      response.success = true;
      response.data = user;
    } else {
      console.error("Cannot find user " + req.params['id'])
    }

    res.send(response);
  });

  router.post('/', isAuthenticated, hasUserWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /user post");
    res.setHeader('Content-Type', 'application/json');
    let user = Users.loadFromPost(req.body);
    user.id = req.params['id'];
    response.success = req.userService.insertUser(user);

    res.send(response);
  });

  router.delete('/:id', isAuthenticated, hasUserWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /user delete");
    res.setHeader('Content-Type', 'application/json');
    response.success = req.userService.deleteUser(req.params['id']);

    res.send(response);
  });

  router.put('/:id', isAuthenticated, hasUserWrite, function (req, res) {
    let response = new ApiResponse();
    res.setHeader('Content-Type', 'application/json');
    console.log("Request: /user put");
    
    let user = Users.loadFromPost(req.body);
    user.id = req.params['id'];
    response.success = req.userService.updateUser(user);
    res.send(response);
  });

  return router;
};