"use strict";

const express = require('express');
const router = express.Router();


module.exports = function (staticRoute) {
  router.get('/dashboard', function (req, res) {
    res.sendFile(staticRoute + '/index.html')
  });
  router.get('/login', function (req, res) {
    res.sendFile(staticRoute + '/index.html')
  });
  router.get('/admin/*', function (req, res) {
    res.sendFile(staticRoute + '/index.html');
  });
  router.get('/profile', function (req, res) {
    res.sendFile(staticRoute + '/index.html');
  });
  router.get('/groups*', function (req, res) {
    res.sendFile(staticRoute + '/index.html');
  });
  router.get('/channels*', function (req, res) {
    res.sendFile(staticRoute + '/index.html');
  });

  return router;
};