"use strict";

const GROUP_PERM = 'group';
const ADMIN_PERM = 'admin';

const express = require('express');
const router = express.Router();
const ApiResponse = require("../models/api_response");
const isAuthenticated = require("../includes/auth").isAuthenticated;
const hasChannelWrite = require("../includes/auth").hasChannelWrite;
const Channels = require("../models/channels");


function checkUserAuth(data, thisUser) {
  if (data.users.includes(thisUser.id)) {
    return true;
  }
  if (thisUser.permissions.includes(ADMIN_PERM)) {
    return true;
  }
  if (thisUser.permissions.includes(GROUP_PERM)) {
    return true;
  }
  return false;
}

module.exports = function (db) {
  router.get('/', isAuthenticated, function (req, res) {
    console.log("Request: /channel get");
    res.setHeader('Content-Type', 'application/json');
    let response = new ApiResponse();
    response.data = req.channelService.getChannels().filter((data) => {
      return checkUserAuth(data, req.thisUser);
    });
    response.success = true;

    res.send(response);

  });

  router.get('/:id', isAuthenticated, function (req, res) {
    console.log("Request: /channel get");
    res.setHeader('Content-Type', 'application/json');
    let response = new ApiResponse();
    
    response.data = {'channel': req.channelService.getChannel(req.params['id'])};
    response.data.group = req.groupService.getGroup(response.data.channel.groupId);
    if (!checkUserAuth(response.data.channel, req.thisUser)) {
      response.data = {channel: null, group: null};
    } else {
      response.success = true;
    }

    res.send(response);

  });

  router.post('/', isAuthenticated, hasChannelWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /channel post");
    res.setHeader('Content-Type', 'application/json');

    let channel = Channels.loadFromPost(req.body);
    response.success = req.channelService.insertChannel(channel);

    res.send(response);
  });

  router.delete('/:id', isAuthenticated, hasChannelWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /channel delete");
    res.setHeader('Content-Type', 'application/json');
    response.success = res.channelService.deleteChannel(req.params['id']);

    res.send(response);
  });

  router.put('/:id', isAuthenticated, hasChannelWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /channel put");
    res.setHeader('Content-Type', 'application/json');

    let channel = Channels.loadFromPost(req.body);
    channel.id = req.params['id'];
    response.success = req.channelService.updateChannel(channel);

    res.send(response);
  });

  return router;
};