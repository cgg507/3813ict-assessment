"use strict";

const GROUP_PERM = 'group';
const ADMIN_PERM = 'admin';

const express = require('express');
const router = express.Router();
const ApiResponse = require("../models/api_response");
const isAuthenticated = require("../includes/auth").isAuthenticated;
const hasGroupWrite = require("../includes/auth").hasGroupWrite;
const Groups = require("../models/groups");

function checkUserAuth(data, thisUser) {
  if (data.users.includes(thisUser.id)) {
    return true;
  }
  if (thisUser.permissions.includes(ADMIN_PERM)) {
    return true;
  }
  if (thisUser.permissions.includes(GROUP_PERM)) {
    if (data.admins.includes(thisUser.id)) {
      return true;
    }
  }
  return false;
}

module.exports = function (db) {
  router.get('/', isAuthenticated, function (req, res) {
    console.log("Request: /group get");
    res.setHeader('Content-Type', 'application/json');
    let response = new ApiResponse();
    let userId = req.params['id'];
    response.data = req.groupService.getGroups().filter((data) => {
      return checkUserAuth(data, req.thisUser);
    });
    response.success = true;

    res.send(response);
  });

  router.get('/:id', isAuthenticated, function (req, res) {
    console.log("Request: /group/:id get");
    res.setHeader('Content-Type', 'application/json');
    let response = new ApiResponse();
    let id = parseInt(req.params['id']);
    if (id > 0) {
      response.data = {group: req.groupService.getGroup(id)};
      if (checkUserAuth(response.data.group, req.thisUser)) {
        response.data.channels = req.channelService.getChannelsForGroup(response.data.group.id);
        response.data.users = req.userService.getUsersForGroup(response.data.group.id);
        response.success = true;
      } else {
        response.data.group = null;
      }
    }
    res.send(response);
  });

  router.post('/', isAuthenticated, hasGroupWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /group post");
    res.setHeader('Content-Type', 'application/json');

    let group = Groups.loadFromPost(req.body);
    response.success = req.groupService.insertGroup(group);

    res.send(response);
  });

  router.delete('/:id', isAuthenticated, hasGroupWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /group/:id delete");
    res.setHeader('Content-Type', 'application/json');

    let id = parseInt(req.params['id']);
    if (id > 0) {
      response.success = req.groupService.deleteGroup(id);
    }

    res.send(response);
  });

  router.put('/:id', isAuthenticated, hasGroupWrite, function (req, res) {
    let response = new ApiResponse();
    console.log("Request: /group/:id put");
    res.setHeader('Content-Type', 'application/json');

    let group = Groups.loadFromPost(req.body);
    group.id = req.params['id'];
    
    response.success = req.groupService.updateGroup(group);

    res.send(response);
  });

  return router;
};