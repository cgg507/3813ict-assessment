import {Component, OnInit} from '@angular/core';
import {LoginFormComponent} from '../../component/login/loginform.component';
import {UserformComponent} from '../../component/userform/userform.component';
import {Router} from '@angular/router';
import {ApiService} from '../../service/api.service';
import {Users} from '../../models/users';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [UserformComponent]
})
export class ProfileComponent implements OnInit {
  user: Users = new Users();

  constructor(private router: Router, private apiService: ApiService) {}


  ngOnInit() {
    try {
      const userData = JSON.parse(localStorage.getItem(ApiService.USERDATA));
      this.user = userData as Users;
    } catch (err) {
      console.error(err);
    }
  }

  /**
   * Submit the form and updates the current user.
   * @param form UserForm
   */
  submitUser(form) {
    if (this.user.id === null) {
      console.error('There should be user data here');
      return;
    }
    // TODO: Actually check if this is the right type
    this.apiService.putUser(this.user.id, form).then((data) => {
      if (data.success) {
        alert('User data was successfully saved');
      } else {
        alert('There were errors on the page. Please check your fields and try again');
      }
    });
  }

}
