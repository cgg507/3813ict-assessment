import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../service/api.service';
import {ActivatedRoute} from '@angular/router';

/**
 * This is a list of channels. Allows editing, creation and deletion.
 * @author Charles Galvin<charles@c2dev.com.au>
 */
@Component({
  selector: 'app-channel-list',
  templateUrl: './channel-list.component.html',
  styleUrls: ['./channel-list.component.css']
})
export class ChannelListComponent implements OnInit {
  channels = [];

  constructor(public apiService: ApiService, private route: ActivatedRoute) {
    this.loadChannels();
  }

  /**
   * Loads channel data from API
   */
  loadChannels() {
    this.apiService.getChannels().then((response) => {
      this.channels = response.data;
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      console.log(params);
    });
  }

  /**
   * Sends a request to delete a channel with a given ID.
   * @param id Number
   */
  deleteChannel(id: Number) {
    this.apiService.deleteChannel(id).then((resp) => {
      if (resp.success) {
        alert('Successfully removed channel');
        this.loadChannels();
      } else {
        alert('Failed to delete channel.');
      }
    });
  }

}
