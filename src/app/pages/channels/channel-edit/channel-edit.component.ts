import { Component, OnInit } from '@angular/core';
import {Groups} from '../../../models/groups';
import {Channels} from '../../../models/channels';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../../service/api.service';
import {ChannelForm} from '../../../models/forms/channelform';

/**
 * Page to edit/create channel models.
 * @author Charles Galvin<charles@c2dev.com.au>
 */
@Component({
  selector: 'app-channel-edit',
  templateUrl: './channel-edit.component.html',
  styleUrls: ['./channel-edit.component.css']
})
export class ChannelEditComponent implements OnInit {
  channel: Channels = new Channels();
  group: Groups = new Groups();

  constructor(private router: Router, public apiService: ApiService, private route: ActivatedRoute) {
    const channelId = this.route.snapshot.paramMap.get('id');
    if (channelId) {
      this.apiService.getChannel(channelId).then((resp) => {
        this.channel = resp.data.channel;
        this.group = resp.data.group;
      });
    }
  }

  ngOnInit() {
  }

  /**
   * Function that is fired by the form on submit. Sends user data to API.
   * On success redirects user back to Channel List
   * @param form ChannelForm
   */
  submitChannel(form: ChannelForm) {
    if (this.channel.id === null || typeof(this.channel.id) === 'undefined') {
      this.apiService.postChannel(form).then((data) => {
        if (data.success) {
          alert('Channel was successfully saved');
          this.router.navigate(['/channels']);
        } else {
          alert('There were errors on the page. Please check your fields and try again');
        }
      });
      return;
    }
    // TODO: Actually check if this is the right type
    this.apiService.putChannel(this.channel.id, form).then((data) => {
      if (data.success) {
        alert('Channel was successfully saved');
        this.router.navigate(['/channels']);
      } else {
        alert('There were errors on the page. Please check your fields and try again');
      }
    });
  }

}
