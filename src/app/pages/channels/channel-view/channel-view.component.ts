import { Component, OnInit } from '@angular/core';
import {Messages} from '../../../models/messages';
import {Channels} from '../../../models/channels';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../../service/api.service';

/**
 * A page to view channel messages
 * @author Charles Galvin<charles@c2dev.com.au>
 */
@Component({
  selector: 'app-channel-view',
  templateUrl: './channel-view.component.html',
  styleUrls: ['./channel-view.component.css']
})
export class ChannelViewComponent implements OnInit {
  channel: Channels = new Channels();
  messages: Messages[] = [];

  constructor(private router: Router, public apiService: ApiService, private route: ActivatedRoute) {
    const channelId = this.route.snapshot.paramMap.get('id');
    if (channelId) {
      this.apiService.getChannel(channelId).then((resp) => {
        this.channel = resp.data.channel;
      });
    }
  }

  ngOnInit() {
  }

}
