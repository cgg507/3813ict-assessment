import { Component, OnInit } from '@angular/core';
import {Groups} from '../../models/groups';
import {Router} from '@angular/router';
import {ApiService} from '../../service/api.service';

/**
 * Dashboard view to show all current channels
 * @author Charles Galvin<charles@c2dev.com.au>
 */
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  /** @var groups Groups */
  groups = [];

  constructor(private router: Router, private apiService: ApiService) {
    this.getGroups();
  }

  /**
   * Get all the groups for this user
   */
  getGroups() {
    this.apiService.getGroups().then((resp) => {
      if (resp.success) {
        this.groups = resp.data;
      } else {
        console.error('Failure to get group data');
      }
    });
  }

  /**
   * Get more detailed information for each group
   * @param group
   */
  getChannelsForGroups(group) {
    this.apiService.getGroup(group.id).then((resp) => {
      if (resp.success) {
        return resp.data;
      } else {
        console.error('Failure to get group data');
      }
    });
  }

  ngOnInit() {
  }

}
