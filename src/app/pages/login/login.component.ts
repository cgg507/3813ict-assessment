import {Component, Input, OnInit} from '@angular/core';
import {LoginFormComponent} from '../../component/login/loginform.component';
import {Router} from '@angular/router';
import {ApiService} from '../../service/api.service';

/**
 * Login form page.
 * @author Charles Galvin<charles@c2dev.com.au>
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginFormComponent]
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private apiService: ApiService) { }

  ngOnInit() {
  }

  /**
   * Submit the form. Redirects to the dashboard on success.
   * @param form LoginForm
   */
  submitLogin(form) {
    const pageRef = this;
    // TODO: Actually check if this is the right type
    this.apiService.postLogin(form).then(function(data) {
      if (data.success) {
        alert('There was a good login. Makes me warm and fuzzy');
        pageRef.router.navigate(['/dashboard']);
      } else {
        alert('Login was no good. Being rejected hurts :\'(');
      }
    });
  }

}
