import { Component, OnInit } from '@angular/core';
import {Users} from '../../../../models/users';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../../../service/api.service';


/**
 * This is the user create/edit page for the admin functionaility.
 * @author Charles Galvin<charles@c2dev.com.au>
 */
@Component({
  selector: 'app-adminedit',
  templateUrl: './adminedit.component.html',
  styleUrls: ['./adminedit.component.css']
})
export class AdmineditComponent implements OnInit {
  user: Users = new Users();

  constructor(private router: Router, private apiService: ApiService, private route: ActivatedRoute) {
    const userId = this.route.snapshot.paramMap.get('id');
    if (userId) {
      this.apiService.getUser(userId).then((resp) => {
        this.user = resp.data;
      });
    }
  }


  ngOnInit() {
  }

  /**
   * Submit the form. On success redirects the user back to the user list.
   * @param form UserForm
   */
  submitUser(form) {
    if (this.user.id === null) {
      this.apiService.postUser(form).then((data) => {
        if (data.success) {
          alert('User data was successfully saved');
          this.router.navigate(['/admin/users']);
        } else {
          alert('There were errors on the page. Please check your fields and try again');
        }
      });
      return;
    }
    // TODO: Actually check if this is the right type
    this.apiService.putUser(this.user.id, form).then((data) => {
      if (data.success) {
        alert('User data was successfully saved');
        this.router.navigate(['/admin/users']);
      } else {
        alert('There were errors on the page. Please check your fields and try again');
      }
    });
  }

}
