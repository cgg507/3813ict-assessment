import { Component, OnInit } from '@angular/core';
import {Users} from '../../../../models/users';
import {ApiService} from '../../../../service/api.service';


/**
 * This is the user list for the admin functionaility.
 * @author Charles Galvin<charles@c2dev.com.au>
 */
@Component({
  selector: 'app-adminlist',
  templateUrl: './adminlist.component.html',
  styleUrls: ['./adminlist.component.css'],
})
export class AdminlistComponent implements OnInit {
  users: Users[];
  showUsers = false;

  constructor(public apiService: ApiService) {
    this.users = [] as Users[];
    this.loadUserList();
  }

  ngOnInit() {
  }

  loadUserList() {
    this.apiService.getUsers().then((response) => {
      this.users = response.data;
    });
  }


  /**
   * Delete the user by id. On success reloads the user list.
   * @param id Number the user id to delete
   */
  deleteUser(id: Number) {
    this.apiService.deleteUser(id).then((data) => {
      if (data.success) {
        alert('User was successfully deleted');
        this.loadUserList();
      } else {
        alert('There was an error deleting the user. Please check server logs.');
      }
    });
  }

}
