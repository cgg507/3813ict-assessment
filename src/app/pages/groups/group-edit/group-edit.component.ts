import { Component, OnInit } from '@angular/core';
import {Users} from '../../../models/users';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../../service/api.service';
import {Groups} from '../../../models/groups';
import {Channels} from '../../../models/channels';
import {GroupForm} from '../../../models/forms/groupform';

@Component({
  selector: 'app-group-edit',
  templateUrl: './group-edit.component.html',
  styleUrls: ['./group-edit.component.css']
})
export class GroupEditComponent implements OnInit {
  group: Groups = new Groups();
  channels: Channels[] = [];
  users: Users[] = [];

  constructor(private router: Router, private apiService: ApiService, private route: ActivatedRoute) {
    const groupId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    if (groupId) {
      this.apiService.getGroup(groupId).then((resp) => {
        this.group = resp.data.group;
        this.channels = resp.data.channels;
        this.users = resp.data.users;
      });
    }
  }

  ngOnInit() {
  }

  /**
   * This is triggered by the form to submit the data to the api.
   * Redirects back to the groups list on success.
   * @param form GroupForm
   */
  submitGroup(form: GroupForm) {
    if (this.group.id === null) {
      this.apiService.postGroup(form).then((data) => {
        if (data.success) {
          alert('Group data was successfully saved');
          this.router.navigate(['/groups']);
        } else {
          alert('There were errors on the page. Please check your fields and try again');
        }
      });
      return;
    }
    // TODO: Actually check if this is the right type
    this.apiService.putGroup(this.group.id, form).then((data) => {
      if (data.success) {
        alert('Group data was successfully saved');
        this.router.navigate(['/groups']);
      } else {
        alert('There were errors on the page. Please check your fields and try again');
      }
    });
  }

}
