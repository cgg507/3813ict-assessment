import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Groups} from '../../models/groups';
import {GroupForm} from '../../models/forms/groupform';
import {Users} from '../../models/users';
import {ApiService} from '../../service/api.service';

@Component({
  selector: 'app-groupform',
  templateUrl: './groupform.component.html',
  styleUrls: ['./groupform.component.css'],
  providers: [ApiService]
})
export class GroupformComponent implements OnInit {
  public groupForm: FormGroup;
  private _group = new Groups();
  public users: Users[];
  @Output() submitForm = new EventEmitter();
  @Input()
  set group(g: Groups) {
    if (typeof(this.groupForm) !== 'undefined') {
      this._group = g;
      this.groupForm.patchValue(g);
    }
  }

  constructor(private formBuilder: FormBuilder, public apiService: ApiService) {
    // Get a list of users
    this.apiService.getUsers().then((resp) => {
      if (resp.success) {
        this.users = resp.data;
      }
    });
  }

  ngOnInit() {
    this.groupForm = this.formBuilder.group({
      name: [this._group.name, [Validators.required]],
      admins: [this._group.admins, []],
      users: [this._group.users, []],
      channels: [this._group.channels, []]
    });
  }

  successForm() {
    const form = new GroupForm();
    form.name = this.groupForm.get('name').value;
    form.admins = this.groupForm.get('admins').value;
    form.users = this.groupForm.get('users').value;
    this.submitForm.emit(form);
  }

}
