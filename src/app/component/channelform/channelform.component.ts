import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Groups} from '../../models/groups';
import {Users} from '../../models/users';
import {ApiService} from '../../service/api.service';
import {GroupForm} from '../../models/forms/groupform';
import {Channels} from '../../models/channels';
import {ChannelForm} from '../../models/forms/channelform';

@Component({
  selector: 'app-channelform',
  templateUrl: './channelform.component.html',
  styleUrls: ['./channelform.component.css']
})
export class ChannelformComponent implements OnInit {
  public channelForm: FormGroup;
  public groups: Groups[] = [];
  public userList: Users[] = [];
  private _channel = new Channels();
  @Output() submitForm = new EventEmitter();
  @Input()
  set channel(c: Channels) {
    if (typeof(this.channelForm) !== 'undefined') {
      this._channel = c;
      this.updateUserList(c.groupId);
      this.channelForm.patchValue(c);
    }
  }

  constructor(private formBuilder: FormBuilder, public apiService: ApiService) {
    // Get a list of users
    this.apiService.getGroups().then((resp) => {
      if (resp.success) {
        this.groups = resp.data;
      }
    });
  }

  ngOnInit() {
    this.channelForm = this.formBuilder.group({
      name: [this._channel.name, [Validators.required]],
      users: [this._channel.users, []],
      groupId: [this._channel.groupId, [Validators.required]]
    });
  }

  successForm() {
    const form = new ChannelForm();

    form.name = this.channelForm.get('name').value;
    form.users = this.channelForm.get('users').value;
    form.groupId = this.channelForm.get('groupId').value;

    this.submitForm.emit(form);
  }

  groupChange(event) {
    this.updateUserList(parseInt(event.target.value, 10));
  }

  updateUserList(id) {
    this.apiService.getGroup(id).then((resp) => {
      if (resp.success) {
        this.userList = resp.data.users;
      }
    });
  }

}
