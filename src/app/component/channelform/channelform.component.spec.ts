import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelformComponent } from './channelform.component';

describe('ChannelformComponent', () => {
  let component: ChannelformComponent;
  let fixture: ComponentFixture<ChannelformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannelformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
