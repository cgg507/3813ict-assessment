import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserForm} from '../../models/forms/userform';
import {Users} from '../../models/users';
import {Groups} from '../../models/groups';

@Component({
  selector: 'app-userform',
  templateUrl: './userform.component.html',
  styleUrls: ['./userform.component.css']
})
export class UserformComponent implements OnInit {
  userForm: FormGroup;
  @Output() submitForm = new EventEmitter();
  private _user = new Users();
  @Input()
  set user(u: Users) {
    if (typeof(this.userForm) !== 'undefined') {
      this._user = u;
      this.userForm.patchValue(u);
    }
  }

  constructor(private formBuilder: FormBuilder) {
    this.userForm = this.formBuilder.group({
      email: [this._user.email, [Validators.required, Validators.email]],
      firstName: [this._user.firstName, [Validators.required]],
      lastName: [this._user.lastName, [Validators.required]],
    });
  }

  ngOnInit() { }

  successForm() {
    const form = new UserForm();

    form.email = this.userForm.get('email').value;
    form.firstName = this.userForm.get('firstName').value;
    form.lastName = this.userForm.get('lastName').value;
    this.submitForm.emit(form);
  }

}
