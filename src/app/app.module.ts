import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { PublicComponent } from './layouts/public/public.component';
import { SecureComponent } from './layouts/secure/secure.component';

import {RouterModule} from '@angular/router';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {PUBLIC_ROUTES} from './routes/public.routes';
import {SECURE_ROUTES} from './routes/secure.routes';
import {SocketService} from './service/socket.service';
import {ApiService} from './service/api.service';
import {LoginFormComponent} from './component/login/loginform.component';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { AdminlistComponent } from './pages/admin/users/adminlist/adminlist.component';
import { AdmineditComponent } from './pages/admin/users/adminedit/adminedit.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { UserformComponent } from './component/userform/userform.component';
import { GroupListComponent } from './pages/groups/group-list/group-list.component';
import { GroupEditComponent } from './pages/groups/group-edit/group-edit.component';
import { GroupformComponent } from './component/groupform/groupform.component';
import { ChannelViewComponent } from './pages/channels/channel-view/channel-view.component';
import { ChannelEditComponent } from './pages/channels/channel-edit/channel-edit.component';
import { ChannelListComponent } from './pages/channels/channel-list/channel-list.component';
import { ChannelformComponent } from './component/channelform/channelform.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    PublicComponent,
    SecureComponent,
    LoginFormComponent,
    AdminlistComponent,
    AdmineditComponent,
    ProfileComponent,
    UserformComponent,
    GroupListComponent,
    GroupEditComponent,
    GroupformComponent,
    ChannelViewComponent,
    ChannelEditComponent,
    ChannelListComponent,
    ChannelformComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
        { path: '', component: LoginComponent},
        { path: '', component: PublicComponent, children: PUBLIC_ROUTES },
        { path: '', component: SecureComponent, children: SECURE_ROUTES }
    ])
  ],
  providers: [SocketService, ApiService, FormBuilder],
  bootstrap: [AppComponent]
})
export class AppModule { }
