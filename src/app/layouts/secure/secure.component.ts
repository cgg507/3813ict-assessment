import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../../service/api.service';
import {Messages} from '../../models/messages';
import {Users} from '../../models/users';

@Component({
  selector: 'app-secure',
  templateUrl: './secure.component.html',
  styleUrls: ['./secure.component.css']
})
export class SecureComponent implements OnInit {
  messages = 0;
  messageList: [Messages];
  user: Users = null;

  constructor(private route: Router, public api: ApiService) {
    try {
      const jsonData = JSON.parse(window.localStorage.getItem(ApiService.USERDATA));
      this.user = jsonData as Users;
    } catch (err) {
      console.error(err.messages);
    }
  }

  ngOnInit() {
  }

  signout() {
    this.api.getLogout().then((resp) => {
      window.localStorage.removeItem(ApiService.USERTOKEN);
      this.route.navigate(['/login']);
    });
  }

}
