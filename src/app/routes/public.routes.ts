import {Routes} from "@angular/router";
import {LoginComponent} from "../pages/login/login.component";

export const PUBLIC_ROUTES: Routes = [
  {path: 'login', component: LoginComponent}
];