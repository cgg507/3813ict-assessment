import {Routes} from '@angular/router';
import {DashboardComponent} from '../pages/dashboard/dashboard.component';
import {ProfileComponent} from '../pages/profile/profile.component';
import {SessionCheckService} from '../service/session-check.service';
import {GroupListComponent} from '../pages/groups/group-list/group-list.component';
import {GroupEditComponent} from '../pages/groups/group-edit/group-edit.component';
import {ChannelViewComponent} from '../pages/channels/channel-view/channel-view.component';
import {ChannelListComponent} from '../pages/channels/channel-list/channel-list.component';
import {ChannelEditComponent} from '../pages/channels/channel-edit/channel-edit.component';
import {AdmineditComponent} from '../pages/admin/users/adminedit/adminedit.component';
import {AdminlistComponent} from '../pages/admin/users/adminlist/adminlist.component';

export const SECURE_ROUTES: Routes = [
  {path: 'dashboard', component: DashboardComponent, canActivate: [SessionCheckService]}, // canActivate: [className]
  {path: 'admin/users', component: AdminlistComponent, canActivate: [SessionCheckService]},
  {path: 'admin/users/create', component: AdmineditComponent, canActivate: [SessionCheckService]},
  {path: 'admin/users/:id', component: AdmineditComponent, canActivate: [SessionCheckService]},
  {path: 'profile', component: ProfileComponent, canActivate: [SessionCheckService]},
  {path: 'groups', component: GroupListComponent, canActivate: [SessionCheckService]},
  {path: 'groups/create', component: GroupEditComponent, canActivate: [SessionCheckService]},
  {path: 'groups/edit/:id', component: GroupEditComponent, canActivate: [SessionCheckService]},
  {path: 'channels', component: ChannelListComponent, canActivate: [SessionCheckService]},
  {path: 'channels/create', component: ChannelEditComponent, canActivate: [SessionCheckService]},
  {path: 'channels/:id', component: ChannelViewComponent, canActivate: [SessionCheckService]},
  {path: 'channels/edit/:id', component: ChannelEditComponent, canActivate: [SessionCheckService]},
];
