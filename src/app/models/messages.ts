import {Users} from './users';

export class Messages {
  id: number = null;
  message: string = null;
  fromUserId: number = null;
  fromUser: Users = null;
  groupId: number = null;
  channelId: number = null;
  createDate: Date = null;
}