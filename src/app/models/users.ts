export class Users {
  id: number = null;
  email: string = null;
  firstName: string = null;
  lastName: string = null;
  permissions: string[] = [];
}