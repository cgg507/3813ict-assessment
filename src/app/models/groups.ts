export class Groups {
  id: number = null;
  name: string = null;
  admins: number[] = [];
  channels: number[] = [];
  users: number[] = [];
}
