import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {LoginForm} from '../models/forms/loginform';
import {
  ApiResponse,
  ChannelResponse, ChannelsResponse,
  GroupResponse,
  GroupsResponse,
  LoginResponse,
  UserResponse, UsersResponse
} from '../models/forms/apiresponse';
import {UserForm} from '../models/forms/userform';
import {GroupForm} from '../models/forms/groupform';
import {Users} from '../models/users';
import {ChannelForm} from '../models/forms/channelform';


/**
 * ApiService
 * This class handles all communication with the API. It also contains some auth functions that check whether the
 * current user has the ability to perform certain actions.
 * @author Charles Galvin<charles@c2dev.com.au>
 */
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  static USERTOKEN = 'usertoken'; // localStorage constant for the user token name
  static USERDATA = 'userData';   // localStorgae constant for the user data
  apiUrl = 'http://localhost:3000'; // The api endpoint

  constructor(public http: HttpClient) {
  }

  /**
   * This performs a get request against the api.
   * @param url string The url we want to talk to
   * @param auth boolean Whether we need to send the auth header
   * @return Promise<Object> The api response as a promise
   */
  private async getRequest(url, auth = true): Promise<Object> {
    const reqOpts = {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      params: new HttpParams()
    };
    if (auth) {
      const token = window.localStorage.getItem(ApiService.USERTOKEN);
      reqOpts.headers['Authorization'] = 'Basic ' + btoa(token);
    }
    return this.http.get(this.apiUrl + url, reqOpts).toPromise();
  }

  /**
   * This performs a post request against the api. Converts `data` to a json object.
   * @param url string The url we want to talk to
   * @param data Object The post data we want to send the API
   * @param auth boolean Whether we need to send the auth header
   * @return Promise<Object> The api response as a promise
   */
  private async postRequest(url, data, auth = true): Promise<Object> {
    const reqOpts = {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      withCredentials: true,
      params: new HttpParams()
    };
    if (auth) {
      const token = window.localStorage.getItem(ApiService.USERTOKEN);
      reqOpts.headers['Authorization'] = 'Basic ' + btoa(token);
    }
    return this.http.post(this.apiUrl + url, JSON.stringify(data), reqOpts).toPromise();
  }

  /**
   * This performs a delete request against the api.
   * @param url string The url we want to talk to
   * @param auth boolean Whether we need to send the auth header
   * @return Promise<Object> The api response as a promise
   */
  private async deleteRequest(url, auth = true): Promise<Object> {
    const reqOpts = {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      withCredentials: true,
      params: new HttpParams()
    };
    if (auth) {
      const token = window.localStorage.getItem(ApiService.USERTOKEN);
      reqOpts.headers['Authorization'] = 'Basic ' + btoa(token);
    }
    return this.http.delete(this.apiUrl + url, reqOpts).toPromise();
  }

  /**
   * This performs a put request against the api. Converts `data` to a json object.
   * @param url string The url we want to talk to
   * @param data Object The post data we want to send the API
   * @param auth boolean Whether we need to send the auth header
   * @return Promise<Object> The api response as a promise
   */
  private async putRequest(url, data, auth = true): Promise<Object> {
    const reqOpts = {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      withCredentials: true,
      params: new HttpParams()
    };
    if (auth) {
      const token = window.localStorage.getItem(ApiService.USERTOKEN);
      reqOpts.headers['Authorization'] = 'Basic ' + btoa(token);
    }
    return this.http.put(this.apiUrl + url, JSON.stringify(data), reqOpts).toPromise();
  }

  /**
   * This sends a request to the login endpoint.
   * @param loginForm LoginForm The login data
   * @return Promise<LoginResponse> The api response as a promise
   */
  postLogin(loginForm: LoginForm): Promise<LoginResponse> {
    return this.postRequest('/api/api/login', loginForm, false).then(function (data) {
      const resp = data as LoginResponse;
      if (resp.success) {
        // Save the token
        if (typeof(resp.data.session) !== undefined) {
          window.localStorage.setItem(ApiService.USERTOKEN, resp.data.session);
        }
        if (typeof(resp.data.user) !== undefined) {
          window.localStorage.setItem(ApiService.USERDATA, JSON.stringify(resp.data.user));
        }
        return resp;
      }
      return resp;
    }).catch(err => {
      // TODO: Deal with it
      return new LoginResponse();
    });
  }

  /**
   * This sends a request to the logout endpoint
   * @return Promise<ApiResponse> The api response as a promise
   */
  getLogout(): Promise<ApiResponse> {
    return this.getRequest('/api/api/logout', true).then(function (data) {
      const resp = data as ApiResponse;
      return resp;
    }).catch(err => {
      // TODO: Deal with it
      return new ApiResponse();
    });
  }

  /**
   * This performs a request against the groups route. Gets all groups the user can access.
   * @return Promise<GroupsResponse> The api response as a promise
   */
  getGroups(): Promise<GroupsResponse> {
    return this.getRequest('/api/group/', true).then(function (data) {
      const resp = data as GroupsResponse;
      return resp;
    }).catch(err => {
      // TODO: Deal with it
      return new GroupsResponse();
    });
  }

  /**
   * This performs a request against the groups route. Gets a group object by the id.
   * @param id Number Group we want to get
   * @return Promise<GroupResponse> The api response as a promise
   */
  getGroup(id: Number): Promise<GroupResponse> {
    return this.getRequest('/api/group/' + id, true).then(function (data) {
      try {
        return data as GroupResponse;
      } catch (err) {
        console.error(err);
        return new GroupResponse();
      }
    }).catch(err => {
      // TODO: Deal with it
      console.error(err);
      return new GroupResponse();
    });
  }

  /**
   * This performs a request against the groups route. Posts a new group object.
   * @param groupForm GroupForm The group we want to create
   * @return Promise<GroupResponse> The api response as a promise
   */
  postGroup(groupForm: GroupForm): Promise<GroupResponse> {
    return this.postRequest('/api/group', groupForm, true).then(function (data) {
      try {
        return data as GroupResponse;
      } catch (err) {
        console.error(err);
        return new GroupResponse();
      }
    }).catch(err => {
      // TODO: Deal with it
      console.error(err);
      return new GroupResponse();
    });
  }

  /**
   * This performs a request against the groups route. Updates a group object using the groupData and id.
   * @param id Number The group we want to update
   * @param groupForm GroupForm The new data for the group object
   * @return Promise<GroupResponse> The api response as a promise
   */
  putGroup(id: Number, groupForm: GroupForm): Promise<GroupResponse> {
    return this.putRequest('/api/group/' + id, groupForm, true).then(function (data) {
      const resp = data as GroupResponse;

      return resp;
    }).catch(err => {
      // TODO: Deal with it
      return new GroupResponse();
    });
  }

  /**
   * This performs a request against the groups route. Deletes a group object by the id.
   * @param id Number The url we want to talk to
   * @return Promise<ApiResponse> The api response as a promise
   */
  deleteGroup(id: Number): Promise<ApiResponse> {
    return this.deleteRequest('/api/group/' + id, true).then(function (data) {
      const resp = data as ApiResponse;
      return resp;
    }).catch(err => {
      return new ApiResponse();
    });
  }

  /**
   * This performs a request against the users route. Gets a user object by the id.
   * @param id Number The user we want to get
   * @return Promise<UserResponse> The api response as a promise
   */
  getUser(id): Promise<UserResponse> {
    return this.getRequest('/api/user/' + id, true).then(function (data) {
      const resp = data as UserResponse;
      return resp;
    }).catch(err => {
      // TODO: Deal with it
      return new UserResponse();
    });
  }

  /**
   * This performs a request against the users route. Gets a users objects.
   * @return Promise<UsersResponse> The api response as a promise
   */
  getUsers(): Promise<UsersResponse> {
    return this.getRequest('/api/user/', true).then(function (data) {
      const resp = data as UsersResponse;
      return resp;
    }).catch(err => {
      // TODO: Deal with it
      return new UsersResponse();
    });
  }

  /**
   * This performs a request against the users route. Creates a user object by the post data.
   * @param userForm UserForm The user we want to create
   * @return Promise<UserResponse> The api response as a promise
   */
  postUser(userForm: UserForm): Promise<UserResponse> {
    return this.postRequest('/api/user', userForm, true).then(function (data) {
      const resp = data as UserResponse;

      return resp;
    }).catch(err => {
      // TODO: Deal with it
      return new UserResponse();
    });
  }

  /**
   * This performs a request against the users route. Updates a user object by the id.
   * @param id Number The user we want to update
   * @param userForm UserForm The new user data
   * @return Promise<UserResponse> The api response as a promise
   */
  putUser(id, userForm: UserForm): Promise<UserResponse> {
    return this.putRequest('/api/user/' + id, userForm, true).then(function (data) {
      const resp = data as UserResponse;

      return resp;
    }).catch(err => {
      // TODO: Deal with it
      return new UserResponse();
    });
  }

  /**
   * This performs a request against the users route. Deletes a User
   * @param id Number Id of the user we want to delete
   * @return Promise<ApiResponse> The api response as a promise
   */
  deleteUser(id): Promise<ApiResponse> {
    return this.deleteRequest('/api/user/' + id, true).then(function (data) {
      try {
        return data as ApiResponse;
      } catch (err) {
        return new ApiResponse();
      }
    }).catch(err => {
      console.error(err);
      return new ApiResponse();
    });
  }

  /**
   * This performs a request against the channels route. Gets a list of channels
   * @param withParam string An extra param to filter the channels
   * @return Promise<ChannelsResponse> The api response as a promise
   */
  getChannels(withParam = ''): Promise<ChannelsResponse> {
    return this.getRequest('/api/channel' + withParam, true).then(function (data) {
      const resp = data as ChannelsResponse;
      return resp;
    }).catch(err => {
      // TODO: Deal with it
      return new ChannelsResponse();
    });
  }

  /**
   * This performs a request against the channels route. Gets a channel by the id
   * @param id Number The id of the channel we want to fetch
   * @return Promise<ChannelResponse> The api response as a promise
   */
  getChannel(id): Promise<ChannelResponse> {
    return this.getRequest('/api/channel/' + id, true).then(function (data) {
      const resp = data as ChannelResponse;
      return resp;
    }).catch(err => {
      // TODO: Deal with it
      return new ChannelResponse();
    });
  }

  /**
   * This performs a request against the channels route. Create a new channel using post data
   * @param channelForm ChannelForm Data of the new channel we want to create
   * @return Promise<ChannelResponse> The api response as a promise
   */
  postChannel(channelForm: ChannelForm): Promise<ChannelResponse> {
    return this.postRequest('/api/channel', channelForm, true).then(function (data) {
      const resp = data as ChannelResponse;

      return resp;
    }).catch(err => {
      // TODO: Deal with it
      return new ChannelResponse();
    });
  }

  /**
   * This performs a request against the channels route. Updates a channel
   * @param id Number The id of the channel we want to update
   * @param channelForm ChannelForm The updated data of the channel object
   * @return Promise<ChannelResponse> The api response as a promise
   */
  putChannel(id, channelForm: ChannelForm): Promise<ChannelResponse> {
    return this.putRequest('/api/channel/' + id, channelForm, true).then(function (data) {
      const resp = data as ChannelResponse;

      return resp;
    }).catch(err => {
      // TODO: Deal with it
      return new ChannelResponse();
    });
  }

  /**
   * This performs a request against the channels route. Deletes a channel
   * @param id Number Id of the channel we want to delete
   * @return Promise<ApiResponse> The api response as a promise
   */
  deleteChannel(id): Promise<ApiResponse> {
    return this.deleteRequest('/api/channel/' + id, true).then(function (data) {
      const resp = data as ApiResponse;
      return resp;
    }).catch(err => {
      console.error(err);
      return new ApiResponse();
    });
  }

  /**
   * Get the user data in localStorage
   *
   * @return Users|null
   */
  getStoredUser() {
    const user = JSON.parse(localStorage.getItem(ApiService.USERDATA));
    if (user != null) {
      return user as Users;
    }
    return null;
  }

  /**
   * Check if the user has a permission level
   * @param action The permission to check
   * @return boolean Whether the user is allowed to or not
   */
  canUser(action) {
    const user = this.getStoredUser();
    if (user != null) {
      // Admin can do anything
      if (user.permissions.includes('admin')) {
        return true;
      }
      if (user.permissions.includes(action)) {
        return true;
      }
    }
    return false;
  }

}
