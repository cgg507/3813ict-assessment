import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ApiService} from './api.service';


/**
 * This class is responsible for checking to see if the user is able to access secure routes.
 * If not it shunts the user off to the login page.
 * @author Charles Galvin<charles@c2dev.com.au>
 */
@Injectable({
  providedIn: 'root'
})
export class SessionCheckService implements CanActivate {

  constructor() { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const userToken = localStorage.getItem(ApiService.USERTOKEN);
    if (userToken != null) {
      return true;
    }
    return false;
  }
}
