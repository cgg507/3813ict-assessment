"use strict";

let express = require('express');
let app = express();
let http = require('http');
let bodyParser = require('body-parser');
let server = http.Server(app);
let ROOT_PATH = __dirname + '/dist/frontend';
let apiRouter = require("./server/routes/api_router");
let userRouter = require("./server/routes/user_router");
let groupRouter = require("./server/routes/group_router");
let channelRouter = require("./server/routes/channel_router");
let staticRouter = require("./server/routes/static_router");

let socketIO = require('socket.io');
let io = socketIO(server);


const PORT = process.env.PORT || 3000;

let fakeDB = require('./server/services/fake_db')();
fakeDB.loadDatabase();

//Attach the DB object to requests
app.use(function (req, res, next) {
  req.db = fakeDB;
  req.userService = require("./server/services/user_service")(fakeDB);
  req.sessionService = require("./server/services/session_service")(fakeDB);
  req.groupService = require("./server/services/group_service")(fakeDB);
  req.channelService = require("./server/services/channel_service")(fakeDB);

  //This fires after the request is done. Allows us to save the data
  res.on("finish", function() {
    fakeDB.saveDatabase();
  });
  next();
});



app.use(express.static(ROOT_PATH));
app.use(bodyParser.json());
app.use('/', staticRouter(ROOT_PATH));
app.use("/api/api", apiRouter());
app.use("/api/user", userRouter());
app.use("/api/group", groupRouter());
app.use("/api/channel", channelRouter());


server.listen(PORT, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log("My First Nodejs Server!");
  console.log("Server listening on: " + host + " port: " + port);
});

io.on('connection', (socket) => {
  socket.on('test', (message) => {
    console.log(message);
  });
});


