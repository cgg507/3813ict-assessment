###Installing Chatterbox

Installing the service is fairly easy. Please note that it is expected that
this will be run in a BASH environment and all following commands are BASH commands

```bash
term> git clone git clone https://cgg507@bitbucket.org/cgg507/3813ict-assessment.git chatterbox
term> cd chatterbox
term> npm install
term> ng build
```

###Running Chatterbox

Run the service by the following command. It will open a service on port 3000.

```bash
term> npm start 

OR

term> ng build && node server
```

You can then open the app by going to http://localhost:3000

###Describe organisation of git repository

There are three main directories inside the GIT repository. These are:
 
server - This contains the node.js Server source. This folder contains the following
   
 - includes: This is any helper code that doesn't fit the pattern of the other folders
  
 - models: This contains all the models used by the server
   
 - routes: This contains all of the routers used in the api. 
   
 - services: This contains the classes that handle reading and writing from the filesystem. 
 
src - This contains the angular source. This gets compiled into dist.

dist - This is the compiled angluar app. This is what the server will serve to the clients.

###Description of data structures

Users: This is the user model
 
 - id: Number - Unique Identifier
 
 - email: string - Email address, used for login credentials
 
 - firstName: string - Users first name
 
 - lastName: string - Users last name
 
 - permissions: string[] - Array of user permissions
 
 Sessions: This is a user session.
 
  - session: string - This is the session identifier
 
  - userId: Number - This is the user who the session belongs to
  
 Groups: This is a object representing a group
 
  - id: Number - This is a unique identifier
 
  - name: string - This is the name of the group
 
  - users: Number[] - This is an array of users who belong to the group
 
  - admins: Number[] - This is an array of users who have admin privledges inside the group
  
 Channels: This is a object representing a channel
 
  - id: Number - This is a unique identifier
 
  - name: string - This is the name of the channel
 
  - groupId: Number - This is thr group the channel belongs to
 
  - users: Number[] - This is the users who belong to the channel
  

###Description of responisbilities of server/client

#####Server Responsibilities
The server is responsible for the following:
 
 - Maintaining user sessions and validating users
 
 - Handling login of users
 
 - Authorising users to access and edit data
 
 - Saving and retreval of data from the file system
 
#####Client Responsibilities
The client is responsible for the following:
 
 - Relaying data back and from the client
 
 - Checking if the user has a valid session
 
 - Storing and updating client details
 
 - Storing and using session tokens

###List of routes, parameters and return values/purpose

There are four main controllers:
 
 - ApiRouter
 
 - UserRouter
 
 - ChannelRouter
 
 - GroupRouter
 
 - StaticRouter

NOTE - All API responses return `ApiResponse`. Some routes may extend this class to contain extra information.

POST   api/api/login     -- Login url. Requires LoginForm as POST data. Returns `LoginResponse`

GET    api/api/logout    -- Logout url. Destroys the users current session. Returns `ApiResponse`

###### Users Endpoints

GET    api/user          -- Get all `Users`. No data input. Returns `ApiResponse` with response.data containing an array of `Users`

GET    api/user/:id      -- Get a `Users`. Expects get `Number`. returns `ApiResponse` with data containing `Users`

PUT    api/user/:id      -- Update a `Users` model. Expects POST data to contain a `UsersForm` and GET parameter to contain `Number`. Returns `ApiResponse`

POST   api/user          -- Insert a `Users` model. Expects POST data to contain a `UsersForm` model. Returns `ApiResponse`

DELETE api/user/:id      -- Delete a `Users` model. Expects `Number` as a GET variable. Returns `ApiResponse`

###### Channels Endpoints

GET    api/channel       -- Get all `Channels`. No data input. Returns `ApiResponse` with response.data containing an array of `Channels`

GET    api/channel/:id   -- Get a `Channels`. Expects get `Number`. returns `ApiResponse` with data containing an object {`Channels`, `Users[]`, `Groups`}

PUT    api/channel/:id   -- Update a `Channels` model. Expects POST data to contain a `ChannelForm` and GET parameter to contain `Number`. Returns `ApiResponse`

POST   api/channel       -- Insert a `Channels` model. Expects POST data to contain a `ChannelForm` model. Returns `ApiResponse`

DELETE api/channel/:id   -- Delete a `Channels` model. Expects `Number` as a GET variable. Returns `ApiResponse`

###### Groups Endpoints

GET    api/group       -- Get all `Groups` models. No data input. Returns `ApiResponse` with response.data containing an array of `Groups`

GET    api/group/:id   -- Get a `Groups`. Expects get `Number`. returns `ApiResponse` with data containing an object {`Channels[]`, `Users[]`, `Groups`}

PUT    api/group/:id   -- Update a `Groups` model. Expects POST data to contain a `GroupForm` and GET parameter to contain `Number`. Returns `ApiResponse`

POST   api/group       -- Insert a `Groups` model. Expects POST data to contain a `GroupForm` model. Returns `ApiResponse`

DELETE api/group/:id   -- Delete a `Groups` model. Expects `Number` as a GET variable. Returns `ApiResponse`






###Angular architecture: components, services, models, routes

The angular architecture is split up into a number of different component types and services:
 
 - pages - These are all the pages created as components. The pages are further divided out into folders based on their functionality e.g. channels, groups, admin
 
 - layouts - These are components that configure the layout. Currently there are two - secure and public. This was done because public should not show logged in menus.
 
 - models - This contains classes that represent the models coming from the API.
 
 - component - This contains components that are reusable. Mostly consisting of reusable forms.
 
 - routes - This contains classes that define the routes. 
 
 - service - This contains the services that the app uses. 